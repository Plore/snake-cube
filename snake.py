import numpy as np

orthogonals = {(1,0,0): [(0,1,0), (0,-1,0), (0,0,1), (0,0,-1)],
               (-1,0,0): [(0,1,0), (0,-1,0), (0,0,1), (0,0,-1)],
               (0,1,0): [(1,0,0), (-1,0,0), (0,0,1), (0,0,-1)],
               (0,-1,0): [(1,0,0), (-1,0,0), (0,0,1), (0,0,-1)],
               (0,0,1): [(1,0,0), (-1,0,0), (0,1,0), (0,-1,0)],
               (0,0,-1): [(1,0,0), (-1,0,0), (0,1,0), (0,-1,0)]}

# hard-coded piece lengths
lengths = [1,3,1,1,1,1,1,1,1,1,1,2,1,3,1,1,1,3,2,1,1,1,1,1,2,2,1,1,1,1,1,1,1,1,2,1,2,1,2,1,3,1,1,2,1,2]

# turns out starting from the other end is easier
lengths = lengths[::-1]

# starting position and grid dimensions are somewhat arbitrary; 
# they need to allow building the cube in any direction
grid = np.zeros(shape=(15,15,15))
start_x = 8
start_y = 8
start_z = 8

grid[start_x][start_y][start_z] = 1

sol = []

def possibilities(pos, v, n, max_x, min_x, max_y, min_y, max_z, min_z):
    """determine if it is possible to finish the cube by going lengths[n] units 
    from pos in direction v at the nth step
    returns: 0|1 -> failure|succes
    """

    l = lengths[n]

    # check for self-overlap
    for m in range(1, l + 1):
        if grid[pos[0] + m * v[0]][pos[1] + m * v[1]][pos[2] + m * v[2]] == 1:
            return 0

    pos_new = (pos[0] + l * v[0], pos[1] + l * v[1], pos[2] + l * v[2])

    # update configuration boundaries
    max_x = max(max_x, pos_new[0])
    min_x = min(min_x, pos_new[0])
    max_y = max(max_y, pos_new[1])
    min_y = min(min_y, pos_new[1])
    max_z = max(max_z, pos_new[2])
    min_z = min(min_z, pos_new[2])

    # check if new configuration is still within 4x4x4 bounds
    if max_x - min_x > 3 or max_y - min_y > 3 or max_z - min_z > 3:
        return 0

    if n == len(lengths) - 2:
        print("almost there ...")

    # last piece fits into bounds -> success
    if n == len(lengths) - 1:
        return 1

    # no immediate failure or success -> fill grid and recurse
    for m in range(1, l + 1):
        grid[pos[0] + m * v[0]][pos[1] + m * v[1]][pos[2] + m * v[2]] = 1

    orthos = orthogonals[v]

    for i in range(len(orthos)):
        o = orthos[i]
        p = possibilities(pos_new, o, n+1, max_x, min_x, max_y, min_y, max_z, min_z)
        if p > 0:
            sol.append(pos_new)
            return p

    # no possible configurations can be reached -> clean up
    for m in range(1, l + 1):
        grid[pos[0] + m * v[0]][pos[1] + m * v[1]][pos[2] + m * v[2]] = 0
    return 0

possibilities((8,8,8), (1,0,0), 0, 8,8,8,8,8,8)
sol = sol[::-1]
print(sol)

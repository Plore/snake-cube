A small script to solve the 4x4x4 version of the [snake cube puzzle](https://en.wikipedia.org/wiki/Snake_cube) by backtracking

####Required programs (tested versions):
  - Python 3.4.3

#####Python 3 libraries:
  - Numpy 1.8.2

